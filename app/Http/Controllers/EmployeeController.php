<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Notification;
use App\Notifications\EmployeeAddNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['companies'] = \App\Models\Company::pluck('name', 'id');
        return view('pages.employee.index', $data);
    }

    public function getData(Request $request)
    {
        $input = $request->all();

        $length = (int)@$input['length'];
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];

        $data = array();
        $e = new \App\Models\Employee();

        $count = $e->count();

        $data['recordsFiltered'] = $count;
        $data['recordsTotal'] = $count;

        if (!empty($search) and !empty($search['value'])) {
            $e = $e->where(function ($query) use ($search) {
                $query->where('first_name', 'like', '%' . $search['value'] . '%');
                $query->where('last_name', 'like', '%' . $search['value'] . '%');
                $query->orWhere('email', 'like', '%' . $search['value'] . '%');
                $query->orWhere('phone', 'like', '%' . $search['value'] . '%');
                $query->orWhereHas('company', function ($query) use ($search) {
                    $query->where('name', 'like', '%' . $search['value'] . '%');
                });
            });
        }
        $data['recordsFiltered'] = $e->count();

        $e_tmp = $e->skip($start)->take($length)->orderBy('id');
        $i = $start + 1;

        foreach ($e_tmp->get() as $row) {
            $d = [];
            $d[] = $i++;
            $d[] = $row->full_name;
            $d[] = '<a class="company-detail" data-id="' . $row->company->id . '">' . $row->company->name . '</a>';
            $d[] = $row->email;
            $d[] = $row->phone;

            $aksi = '<div class="d-flex">
                        <form action="' . route('employee.destroy', $row->id) . '" method="post">
                        <input type="text" value="DELETE" name="_method" hidden>
                        <input type="text" value="' . csrf_token() . '" name="_token" hidden>
                        <button type="button" data-id="' . $row->id . '" class="btn btn-sm btn-warning edit"><span class="btn-label"><i class="fas fa-edit"></i></span></button>
                          <button type="submit" class="btn btn-sm btn-danger delete" style="margin-top: 0.5%"><span class="btn-label"><i
                              class="fas fa-trash"></i></span>
                        </button>

                        </form>
                     </div>';

            $d[] = $aksi;
            $data['data'][] = $d;
        }

        if (empty($data['data'])) {
            $data['recordsTotal'] = $count;
            $data['recordsFiltered'] = 0;
            $data['aaData'] = [];
        }

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['companies'] = \App\Models\Company::pluck('name', 'id');
        return view('pages.employee.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'company_id' => 'required|exists:companies,id',
            'email' => 'email|nullable',
        ]);

        // Redirect to form if validation failed
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Insert data
        $data = \App\Models\Employee::create($request->all());

        // Send Mail Notification

        $company = \App\Models\Company::find($request->company_id);
        Notification::send($company, new EmployeeAddNotification($data));

        // Redirect after store
        if ($data) {
            return redirect('employee')->with('success', 'Successfully created data!');
        } else {
            return redirect('employee')->with('error', 'Fail to create data!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = \App\Models\Employee::find($id);
        $data['companies'] = \App\Models\Company::pluck('name', 'id');
        if ($data['data']) {
            return view('pages.employee.form', $data);
        } else {
            return redirect('employee')->with('status', 'Data Not Found!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validation
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'company_id' => 'required|exists:companies,id',
            'email' => 'email|nullable',
        ]);

        // Redirect to form if validation failed
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Update data
        $data = \App\Models\Employee::find($id);
        $data->fill($request->all());
        $data->save();

        // Redirect after store
        if ($data) {
            return redirect('employee')->with('success', 'Successfully update data!');
        } else {
            return redirect('employee')->with('error', 'Fail to create data!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = \App\Models\Employee::find($id);
        if ($data) {
            $data->delete();
        } else {
            return redirect('employee')->with('status', 'Data Not Found!');
        }

        return redirect()->back()->with('success', 'Data Successfully Deleted!');
    }

    public function getEmployeeDetail($id)
    {
        $data['data'] = \App\Models\Employee::find($id);
        return response()->json($data);
    }
}

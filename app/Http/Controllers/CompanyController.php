<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.company.index');
    }

    public function getData(Request $request)
    {
        $input = $request->all();

        $length = (int)@$input['length'];
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];

        $data = array();
        $e = new \App\Models\Company();

        $count = $e->count();

        $data['recordsFiltered'] = $count;
        $data['recordsTotal'] = $count;

        if (!empty($search) and !empty($search['value'])) {
            $e = $e->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search['value'] . '%');
                $query->orWhere('email', 'like', '%' . $search['value'] . '%');
                $query->orWhere('website', 'like', '%' . $search['value'] . '%');
            });
        }
        $data['recordsFiltered'] = $e->count();

        $e_tmp = $e->skip($start)->take($length)->orderBy('id');
        $i = $start + 1;

        foreach ($e_tmp->get() as $row) {
            $d = [];
            $d[] = $i++;
            $d[] = $row->name;
            $d[] = $row->email;
            if (!empty($row->logo)) {
                $d[] = '<img style="height:100px" src="' . $row->logo . '"></img>';
            } else {
                $d[] = '';
            }
            $d[] = '<a target="_blank" href="' . $row->website . '">' . $row->website . '</a>';

            $aksi = '<div class="d-flex">
                        <form action="' . route('company.destroy', $row->id) . '" method="post">
                        <input type="text" value="DELETE" name="_method" hidden>
                        <input type="text" value="' . csrf_token() . '" name="_token" hidden>
                        <button type="button" data-id="' . $row->id . '" class="btn btn-sm btn-warning edit"><span class="btn-label"><i class="fas fa-edit"></i></span></button>
                          <button type="submit" class="btn btn-sm btn-danger delete" style="margin-top: 0.5%"><span class="btn-label"><i
                              class="fas fa-trash"></i></span>
                        </button>

                        </form>
                     </div>';

            $d[] = $aksi;
            $data['data'][] = $d;
        }

        if (empty($data['data'])) {
            $data['recordsTotal'] = $count;
            $data['recordsFiltered'] = 0;
            $data['aaData'] = [];
        }

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.company.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'email|nullable',
            'website' => 'url|nullable',
        ]);

        // Redirect to form if validation failed
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Insert data
        $data = \App\Models\Company::create($request->all());

        // Store Logo

        if (!empty($request->logo)) {
            $request->validate([
                'logo' => 'required|image',
            ]);

            $imageName = $data->id . time() . '.' . $request->logo->extension();

            $request->logo->storeAs('logo', $imageName, ['disk' => 'public']);

            // Update data

            $data->logo = asset('storage/logo') . '/' . $imageName;
            $data->save();
        }

        // Redirect after store
        if ($data) {
            return redirect('company')->with('success', 'Successfully created data!');
        } else {
            return redirect('company')->with('error', 'Fail to create data!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = \App\Models\Company::find($id);
        if ($data['data']) {
            return view('pages.company.form', $data);
        } else {
            return redirect('company')->with('status', 'Data Not Found!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validation
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'email|nullable',
            'website' => 'url|nullable',
        ]);

        // Redirect to form if validation failed
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        // Update data
        $data = \App\Models\Company::find($id);
        $data->fill($request->all());
        $data->save();

        // Store Logo

        if (!empty($request->logo)) {
            $request->validate([
                'logo' => 'required|image',
            ]);

            $imageName = $data->id . time() . '.' . $request->logo->extension();

            $request->logo->storeAs('logo', $imageName, ['disk' => 'public']);

            // Update data

            $data->logo = asset('storage/logo') . '/' . $imageName;
            $data->save();
        }

        // Redirect after store
        if ($data) {
            return redirect('company')->with('success', 'Successfully update data!');
        } else {
            return redirect('company')->with('error', 'Fail to create data!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = \App\Models\Company::find($id);
        if ($data) {
            $data->delete();
        } else {
            return redirect('employee')->with('status', 'Data Not Found!');
        }

        return redirect()->back()->with('success', 'Data Successfully Deleted!');
    }

    public function getCompanyDetail($id)
    {
        $data['data'] = \App\Models\Company::find($id);
        return response()->json($data);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Company extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = ['name', 'email', 'logo', 'website'];

    protected $appends = ['total_employees'];


    public function employees()
    {
        return $this->hasMany('\App\Models\Employee', 'company_id', 'id');
    }

    public function getTotalEmployeesAttribute()
    {
        return $this->employees->count();
    }
}

@extends('layouts.app')

@section('title', 'Company Form')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        @if (@$data)
                            <h1>Edit Company</h1>
                        @else
                            <h1>Add Company</h1>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Company</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Company</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (@$data)
                                <form role="form" id="form" action="{{ url('company') }}/{{ @$data->id }}"
                                    method="post" class="form form-horizontal" enctype="multipart/form-data">
                                    {{ method_field('PATCH') }}
                                @else
                                    <form role="form" id="form" action="{{ url('company') }}" method="post"
                                        class="form form-horizontal" enctype="multipart/form-data">
                            @endif
                            @csrf
                            <div class="form-group row">
                                <label class="col-md-3 label-control" for="name">Company Name</label>
                                <div class="col-md-9">
                                    <input type="text" id="name" class="form-control" placeholder="GR Tech .." name="name"
                                        value="{{ old('name', @$data->name) }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 label-control" for="email">Company Email</label>
                                <div class="col-md-9">
                                    <input type="email" id="email" class="form-control"
                                        placeholder="info@grtech.com.my .." name="email"
                                        value="{{ old('email', @$data->email) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 label-control" for="logo">Logo</label>
                                <div class="col-md-9">
                                    <label id="logo" class="file center-block" for="logo">
                                        <input type="file" id="logo" accept="image/*" name="logo" class="file-preview">
                                        <span class="file-custom"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 label-control">Preview Logo</label>
                                <div class="col-md-9">
                                    @if (!empty(@$data->logo))
                                        <img src="{{ @$data->logo }}" alt="Preview Logo" id="preview-logo"
                                            class="logo">
                                    @else
                                        <img src="https://via.placeholder.com/200x200" alt="Preview Logo" id="preview-logo"
                                            class="logo">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 label-control" for="website">Website</label>
                                <div class="col-md-9">
                                    <input type="text" id="website" class="form-control" placeholder="grtech.com .."
                                        name="website" value="{{ old('website', @$data->website) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Save" class="btn btn-success float-right ml-1">
                                <input type="reset" value="Reset" class="btn btn-danger float-right">
                            </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('css')
    <style>
        .logo {
            width: 200px;
            height: 200px;
            object-fit: cover;
        }

    </style>
@endpush

@push('js')
    <script>
        function readURL(input, id) {
            console.log(id);
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview-' + id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".file-preview").change(function() {
            var id = $(this).attr('id');
            readURL(this, id);
        });
    </script>
@endpush

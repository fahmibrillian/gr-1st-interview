@extends('layouts.app')

@section('title', 'Company')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Company</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Company</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">All Company</h3>
                                <div class="card-tools mr-0">
                                    <a href="{{ url('company/create') }}" class="btn btn-info btn-sm"
                                        data-card-widget="Add" title="Add">
                                        <i class="fas fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="company" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Logo</th>
                                            <th>Website</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

    <!-- Company Modal -->
    <div class="modal fade" id="edit-modal">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Edit Company</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-dismiss="modal" aria-label="Close"
                                    title="Close">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <form role="form" id="form" action="{{ url('company') }}/{{ @$data->id }}" method="post"
                                class="form form-horizontal" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                @csrf
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="name">Company Name</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name" class="form-control" placeholder="GR Tech .."
                                            name="name" value="{{ old('name', @$data->name) }}" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="email">Company Email</label>
                                    <div class="col-md-9">
                                        <input type="email" id="email" class="form-control"
                                            placeholder="info@grtech.com.my .." name="email"
                                            value="{{ old('email', @$data->email) }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="logo">Logo</label>
                                    <div class="col-md-9">
                                        <label id="logo" class="file center-block" for="logo">
                                            <input type="file" id="logo" accept="image/*" name="logo"
                                                class="file-preview">
                                            <span class="file-custom"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control">Preview Logo</label>
                                    <div class="col-md-9">
                                        @if (!empty(@$data->logo))
                                            <img src="{{ @$data->logo }}" alt="Preview Logo" id="preview-logo"
                                                class="logo">
                                        @else
                                            <img src="https://via.placeholder.com/200x200" alt="Preview Logo"
                                                id="preview-logo" class="logo">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 label-control" for="website">Website</label>
                                    <div class="col-md-9">
                                        <input type="text" id="website" class="form-control" placeholder="grtech.com .."
                                            name="website" value="{{ old('website', @$data->website) }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Update" class="btn btn-success float-right ml-1">
                                    <input type="reset" value="Reset" class="btn btn-danger float-right">
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.Company Modal -->
@endsection

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <style>
        .logo {
            width: 200px;
            height: 200px;
            object-fit: cover;
        }

    </style>
@endpush

@push('js')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>

    <script>
        var tbl;
        $(document).ready(function() {
            tbl = $('#company').DataTable({
                "columnDefs": [{
                        "width": "20px",
                        "targets": 0,
                        "column": "id"
                    },
                    {
                        "width": "100px",
                        "targets": 1,
                        "column": "name"
                    },
                    {
                        "width": "100px",
                        "targets": 2,
                        "column": "email"
                    },
                    {
                        "width": "100px",
                        "targets": 3,
                        "column": "logo"
                    },
                    {
                        "width": "100px",
                        "targets": 4,
                        "column": "website"
                    },
                    {
                        "width": "50px",
                        "targets": 5,
                        "column": "action"
                    },
                ],
                deferRender: true,
                serverSide: true,
                processing: true,
                stateSave: true,
                sort: false,
                "responsive": true,
                "lengthChange": true,
                "autoWidth": false,
                ajax: {
                    url: '{{ url('company/get-data') }}',
                    type: 'GET',
                    global: false,
                    data: function(e) {
                        return e;
                    }
                },
                "scrollX": true
            });
        });

        // Modal Edit
        $(document).on("click", ".edit", function() {
            var company_id = $(this).data('id');


            $.ajax({
                url: "{{ url('company/get-detail') }}/" + company_id,
                type: 'GET',
                dataType: 'json', // added data type
                success: function(res) {
                    $('#form').attr('action', '{{ url('company') }}/' + company_id);
                    $('#preview-logo').attr('src', res.data.logo);
                    $('#name').val(res.data.name);
                    $('#email').val(res.data.email);
                    $('#website').val(res.data.website);
                },
                complete: function() {
                    $('#edit-modal').modal('show');
                }
            });


        });

        function readURL(input, id) {
            console.log(id);
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview-' + id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".file-preview").change(function() {
            var id = $(this).attr('id');
            readURL(this, id);
        });
    </script>
@endpush

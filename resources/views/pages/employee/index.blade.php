@extends('layouts.app')

@section('title', 'Employee')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Employee</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Employee</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">All Employee</h3>
                                <div class="card-tools mr-0">
                                    <a href="{{ url('employee/create') }}" class="btn btn-info btn-sm"
                                        data-card-widget="Add" title="Add">
                                        <i class="fas fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="employee" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Full Name</th>
                                            <th>Company</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->


        <!-- Company Detail Modal -->
        <div class="modal fade" id="company-modal">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Company Detail</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- Profile Image -->
                        <div class="card card-primary">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="company-logo-img img-fluid img-circle"
                                        src="https://via.placeholder.com/200x200" id="company-logo" alt="Company Logo">
                                </div>

                                <h3 class="profile-username text-center" id="company-name">Company Name</h3>

                                <p class="text-muted text-center">Company Name</p>

                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Email</b> <a class="float-right" id="company-email"></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Website</b> <a href="#" target="_blank" class="float-right"
                                            id="company-website"></a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Total Employees</b> <a class="float-right" id="company-total-employees"></a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.Company Modal -->

        <!-- Employee Edit Modal -->
        <div class="modal fade" id="edit-modal">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body p-0">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Edit Employee</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-dismiss="modal" aria-label="Close"
                                        title="Close">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form role="form" id="form" action="{{ url('employee') }}/{{ @$data->id }}"
                                    method="post" class="form form-horizontal" enctype="multipart/form-data">
                                    {{ method_field('PATCH') }}
                                    @csrf
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="first_name">First Name</label>
                                        <div class="col-md-9">
                                            <input type="text" id="first_name" class="form-control" placeholder="Jhon .."
                                                name="first_name" value="{{ old('first_name', @$data->first_name) }}"
                                                required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="last_name">Last Name</label>
                                        <div class="col-md-9">
                                            <input type="text" id="last_name" class="form-control" placeholder="Doe .."
                                                name="last_name" value="{{ old('last_name', @$data->last_name) }}"
                                                required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="company_id">Company</label>
                                        <div class="col-md-9">
                                            {{ Form::select('company_id', $companies, @$data->company_id, ['class' => 'form-control select2bs4', 'required', 'id' => 'company_id']) }}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="email">Email</label>
                                        <div class="col-md-9">
                                            <input type="email" id="email" class="form-control"
                                                placeholder="jhon@something.com .." name="email"
                                                value="{{ old('email', @$data->email) }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="phone">Phone</label>
                                        <div class="col-md-9">
                                            <input type="text" id="phone" class="form-control" placeholder="+62XXXXXXX.."
                                                name="phone" value="{{ old('phone', @$data->phone) }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="Update" class="btn btn-success float-right ml-1">
                                        <input type="reset" value="Reset" class="btn btn-danger float-right">
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.Company Modal -->
    </div>
@endsection

@push('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <style>
        .company-detail {
            cursor: pointer;
        }

        .company-logo-img {
            border: 3px solid #adb5bd;
            margin: 0 auto;
            padding: 3px;
            width: 100px;
            height: 100px;
            object-fit: cover;
        }

    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@push('js')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <!-- Select2 -->
    <script src="{{ asset('assets') }}/plugins/select2/js/select2.full.min.js"></script>
    <script>
        var tbl;
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });

            tbl = $('#employee').DataTable({
                "columnDefs": [{
                        "width": "20px",
                        "targets": 0,
                        "column": "id"
                    },
                    {
                        "width": "100px",
                        "targets": 1,
                        "column": "full_name"
                    },
                    {
                        "width": "100px",
                        "targets": 2,
                        "column": "company"
                    },
                    {
                        "width": "100px",
                        "targets": 3,
                        "column": "email"
                    },
                    {
                        "width": "100px",
                        "targets": 4,
                        "column": "phone"
                    },
                    {
                        "width": "50px",
                        "targets": 5,
                        "column": "action"
                    },
                ],
                deferRender: true,
                serverSide: true,
                processing: true,
                stateSave: true,
                sort: false,
                "responsive": true,
                "lengthChange": true,
                "autoWidth": false,
                ajax: {
                    url: '{{ url('employee/get-data') }}',
                    type: 'GET',
                    global: false,
                    data: function(e) {
                        return e;
                    }
                },
                "scrollX": true
            });
        });

        $(document).on("click", ".company-detail", function() {
            var company_id = $(this).data('id');


            $.ajax({
                url: "{{ url('company/get-detail') }}/" + company_id,
                type: 'GET',
                dataType: 'json', // added data type
                success: function(res) {
                    console.log(res);
                    $('#company-logo').attr('src', res.data.logo);
                    $('#company-name').text(res.data.name);
                    $('#company-email').text(res.data.email);
                    $('#company-website').text(res.data.website);
                    $('#company-website').attr('href', res.data.website);
                    $('#company-total-employees').text(res.data.total_employees);
                },
                complete: function() {
                    $('#company-modal').modal('show');
                }
            });
        });

        // Modal Edit
        $(document).on("click", ".edit", function() {
            var employee_id = $(this).data('id');

            $.ajax({
                url: "{{ url('employee/get-detail') }}/" + employee_id,
                type: 'GET',
                dataType: 'json', // added data type
                success: function(res) {
                    $('#form').attr('action', '{{ url('employee') }}/' + employee_id);
                    $('#first_name').val(res.data.first_name);
                    $('#last_name').val(res.data.last_name);
                    $('#email').val(res.data.email);
                    $('#phone').val(res.data.phone);
                    $('#company_id').val(res.data.company_id);
                    $('#company_id').trigger('change');
                },
                complete: function() {
                    $('#edit-modal').modal('show');
                }
            });
        });
    </script>
@endpush

@extends('layouts.app')

@section('title', 'Employee Form')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        @if (@$data)
                            <h1>Edit Employee</h1>
                        @else
                            <h1>Add Employee</h1>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Employee</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Employee</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (@$data)
                                <form role="form" id="form" action="{{ url('employee') }}/{{ @$data->id }}"
                                    method="post" class="form form-horizontal" enctype="multipart/form-data">
                                    {{ method_field('PATCH') }}
                                @else
                                    <form role="form" id="form" action="{{ url('employee') }}" method="post"
                                        class="form form-horizontal" enctype="multipart/form-data">
                            @endif
                            @csrf
                            <div class="form-group row">
                                <label class="col-md-3 label-control" for="first_name">First Name</label>
                                <div class="col-md-9">
                                    <input type="text" id="first_name" class="form-control" placeholder="Jhon .."
                                        name="first_name" value="{{ old('first_name', @$data->first_name) }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 label-control" for="last_name">Last Name</label>
                                <div class="col-md-9">
                                    <input type="text" id="last_name" class="form-control" placeholder="Doe .."
                                        name="last_name" value="{{ old('last_name', @$data->last_name) }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 label-control" for="company_id">Company</label>
                                <div class="col-md-9">
                                    {{ Form::select('company_id', $companies, @$data->company_id, ['class' => 'form-control select2bs4', 'required']) }}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 label-control" for="email">Email</label>
                                <div class="col-md-9">
                                    <input type="email" id="email" class="form-control"
                                        placeholder="jhon@something.com .." name="email"
                                        value="{{ old('email', @$data->email) }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 label-control" for="phone">Phone</label>
                                <div class="col-md-9">
                                    <input type="text" id="phone" class="form-control" placeholder="+62XXXXXXX.."
                                        name="phone" value="{{ old('phone', @$data->phone) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Save" class="btn btn-success float-right ml-1">
                                <input type="reset" value="Reset" class="btn btn-danger float-right">
                            </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


@endsection

@push('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <style>
        .logo {
            width: 200px;
            height: 200px;
            object-fit: cover;
        }

    </style>
@endpush

@push('js')
    <!-- Select2 -->
    <script src="{{ asset('assets') }}/plugins/select2/js/select2.full.min.js"></script>
    <script>
        function readURL(input, id) {
            console.log(id);
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#preview-' + id).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".file-preview").change(function() {
            var id = $(this).attr('id');
            readURL(this, id);
        });

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    </script>
@endpush

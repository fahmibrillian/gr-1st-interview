@push('js')
    @if (count($errors) > 0)
        <script>
            var error = {!! $errors !!}
            var err = ''
            for (var key in error) {
                err += error[key].join("<br>") + "<br>"
            }
            toastr.error(err, 'error', {
                "showMethod": "slideDown",
                "hideMethod": "slideUp",
                timeOut: 2000,
                "positionClass": "toast-bottom-right",
            });
        </script>
    @endif
    @if (session('success') && !is_array(session('success')))
        <script>
            toastr.success('{!! session('success') !!}', 'Success', {
                "showMethod": "slideDown",
                "hideMethod": "slideUp",
                timeOut: 40000,
                "positionClass": "toast-bottom-right",
            });
        </script>
    @endif

    @if (session('status') && !is_array(session('status')))
        <script>
            toastr.info('{!! session('status') !!}', 'Info', {
                "showMethod": "slideDown",
                "hideMethod": "slideUp",
                timeOut: 40000,
                "positionClass": "toast-bottom-right",
            });
        </script>
    @endif
    @if (session('error') && !is_array(session('error')))
        <script>
            toastr.error('{!! session('error') !!}', 'Error', {
                "showMethod": "slideDown",
                "hideMethod": "slideUp",
                timeOut: 40000,
                "positionClass": "toast-bottom-right",
            });
        </script>
    @endif

    @if (session('message') && !is_array(session('message')))
        <script>
            toastr.success('{!! session('message') !!}', 'Success', {
                "showMethod": "slideDown",
                "hideMethod": "slideUp",
                timeOut: 40000,
                "positionClass": "toast-bottom-right",
            });
        </script>
    @endif

    <script>
        $("button[type='reset']").on("click", function(event) {
            event.preventDefault();
            $('input,select,textarea').not('[readonly],[disabled],:button,:hidden').val('');

            //Reset Readonly Total
            $('input[name=total]').val(0);

            //Reset Repeater List
            $('[data-repeater-list]').empty();
            $('[data-repeater-create]').click();

            //Hide Btn Delete Ketika Reset
            // $('.btn-danger').hide();
        });

        $('body').on('click', '.delete', function(e) {
            e.preventDefault();
            var form = $(this).closest('form');
            Swal.fire({
                    title: "Confirmation?",
                    text: "Are you sure want to delete this data?",
                    icon: "warning",
                    showCancelButton: true,
                    cancelButtonColor: '#3085d6',
                    confirmButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        form.submit();
                    }
                });
        });
    </script>
@endpush

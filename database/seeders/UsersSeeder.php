<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = [
            [
                'name' => 'Admin',
                'email' => 'admin@grtech.com.my',
                'password' => bcrypt('password'),
                'role' => 'admin'
            ],
            [
                'name' => 'User',
                'email' => 'user@grtech.com.my',
                'password' => bcrypt('password'),
                'role' => 'user'
            ],
        ];

        foreach ($user as $key => $value) {
            DB::table('users')->insert($value);
        }
    }
}

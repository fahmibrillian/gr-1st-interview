<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes(['register' => false]);


// All user Route
Route::get('/', function () {
    return redirect('login');
});

// Admin Only Route
Route::group(['middleware' => ['admin']], function () {

    // Company Route
    Route::get('/company/get-data', [\App\Http\Controllers\CompanyController::class, 'getData']);
    Route::get('/company/get-detail/{id}', [\App\Http\Controllers\CompanyController::class, 'getCompanyDetail']);
    Route::resource('company', '\App\Http\Controllers\CompanyController');

    // Employee Route
    Route::get('/employee/get-data', [\App\Http\Controllers\EmployeeController::class, 'getData']);
    Route::get('/employee/get-detail/{id}', [\App\Http\Controllers\EmployeeController::class, 'getEmployeeDetail']);
    Route::resource('employee', '\App\Http\Controllers\EmployeeController');
});

// Authenticated Route
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});
